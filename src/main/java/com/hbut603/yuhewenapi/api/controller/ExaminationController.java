package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.Examination;
import com.hbut603.yuhewenapi.api.service.IExaminationService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 * 试题表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/examination")
public class ExaminationController {
    @Autowired
    private IExaminationService examService;

    @ApiOperation("查询所有试题")
    @GetMapping("/findall")
    public Object findAll() {
        List<Examination> examninations = examService.list();
        return new JsonData().buildSuccess(examninations, 200);
    }

    @ApiOperation("查询单个试题")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "E_id")  String account) {
        QueryWrapper queryWrapper = new QueryWrapper<Examination>();
        Examination examination = examService.getOne(queryWrapper.eq(true,"examination_account",account));
        return new JsonData().buildSuccess(examination, 200);
    }

    @ApiOperation("新增试题")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody Examination examination) {
        boolean isSuccess = examService.save(examination);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个试题信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个试题")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "E_id")  String account) {
        QueryWrapper queryWrapper = new QueryWrapper<Examination>();
        boolean isSuccess = examService.remove(queryWrapper.eq(true,"examination_account",account));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
}

