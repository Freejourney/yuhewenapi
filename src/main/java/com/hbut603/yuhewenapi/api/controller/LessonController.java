package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.Lesson;
import com.hbut603.yuhewenapi.api.service.ILessonService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 课程表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/lesson")
public class LessonController {
	@Autowired
    private ILessonService lessonService;

    @ApiOperation("查询所有课程信息")
    @GetMapping("/findall")
    public Object findAll() {
        List<Lesson> lesson = lessonService.list();
        return new JsonData().buildSuccess(lesson, 200);
    }

    @ApiOperation("查询单个课程信息")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "code")  String code) {
        QueryWrapper queryWrapper = new QueryWrapper<Lesson>();
        Lesson lesson = lessonService.getOne(queryWrapper.eq(true,"lesson_code",code));
        return new JsonData().buildSuccess(lesson, 200);
    }

    @ApiOperation("新增课程")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody Lesson lesson) {
        boolean isSuccess = lessonService.save(lesson);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个课程信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个课程")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "code")  String code) {
        QueryWrapper queryWrapper = new QueryWrapper<Lesson>();
        boolean isSuccess = lessonService.remove(queryWrapper.eq(true,"lesson_code",code));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }

    @ApiOperation("按种类查询课程信息")
    @GetMapping("/findbytype")
    public Object findByType(@RequestParam(value = "type")  String type) {
        QueryWrapper queryWrapper = new QueryWrapper<Lesson>();
        List<Lesson> lesson = lessonService.listObjs(queryWrapper.eq(true,"lesson_type",type));
        return new JsonData().buildSuccess(lesson, 200);
    }
}