package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.Register;
import com.hbut603.yuhewenapi.api.entity.User;
import com.hbut603.yuhewenapi.api.service.IUserService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 用户信息表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private IUserService userService;

    @ApiOperation("登录查询login")
    @GetMapping("/login")
    public Object login(@RequestParam(value="tel") String userId, @RequestParam(value="passwords") String pwd){
        QueryWrapper queryWrapper = new QueryWrapper<User>();
        User user = userService.getOne(queryWrapper.eq(true,"user_account",userId));
        Map<String,String> info = new HashMap<>();
        if (user.getUserPassword().equals(pwd)) {
            info.put("username", user.getUserName());
            info.put("userid", user.getUserAccount());
            info.put("message", "login success");
            return new JsonData().buildSuccess(info, 200);
        } else {
            info.put("message", "login fail");
            return new JsonData().buildSuccess(info, 200);
        }
    }

    @ApiOperation("查询所有用户")
    @GetMapping("/findall")
    public Object findAll() {
        List<User> users = userService.list();
        return new JsonData().buildSuccess(users, 200);
    }

    @ApiOperation("查询单个用户")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "account")  String account) {
        QueryWrapper queryWrapper = new QueryWrapper<User>();
        User user = userService.getOne(queryWrapper.eq(true,"user_account",account));
        return new JsonData().buildSuccess(user, 200);
    }

    @ApiOperation("新增用户")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody Register register) {
        User user = new User();
        user.setUserEmail(register.getMail());
        user.setUserName(register.getName());
        user.setUserNickname(register.getUsername());
        user.setUserPassword(register.getPasswords());
        user.setUserPhone(register.getTel());
        user.setUserAccount(register.getTel());
        user.setUserRole(3);
        user.setAccountBalance(BigDecimal.valueOf(12));
        user.setPasswordSalt("123");
        user.setUserCreateTime(LocalDateTime.now());
        user.setUserUpdateTime(LocalDateTime.now());
        boolean isSuccess = userService.save(user);
        Map<String,Object> info = new HashMap<>();
        info.put("success", true);
        return new JsonData().buildSuccess(info,0);
    }

    @ApiOperation("修改单个用户信息")
    @GetMapping("/update")
    public Object update(@RequestParam(value = "passwords") String password,@RequestParam(value = "tel")  String account) {

        QueryWrapper queryWrapper = new QueryWrapper<User>();
        User olduser = userService.getOne(queryWrapper.eq(true,"user_account",account));
        olduser.setUserPassword(password);
        boolean isSuccess = userService.update(olduser,queryWrapper.eq(true,"user_account",account));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }

    @ApiOperation("删除单个用户")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "account")  String account) {
        QueryWrapper queryWrapper = new QueryWrapper<User>();
        boolean isSuccess = userService.remove(queryWrapper.eq(true,"user_account",account));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
}

