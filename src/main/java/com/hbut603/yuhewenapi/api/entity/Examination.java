package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 试题表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="Examination对象", description="试题表")
public class Examination extends Model<Examination> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "exam_id", type = IdType.AUTO)
    private Integer examId;

    @ApiModelProperty(value = "试题编号")
    private String examCode;

    @ApiModelProperty(value = "试题答案")
    private String examAnswer;

    @ApiModelProperty(value = "试题年级")
    private String examGrade;

    @ApiModelProperty(value = "试题类型")
    private String examType;

    @ApiModelProperty(value = "试题难度")
    private String examHardness;

    @ApiModelProperty(value = "选项")
    private String examOptions;

    @ApiModelProperty(value = "创建时间")
    private String examCreateTime;


    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }

    public String getExamCode() {
        return examCode;
    }

    public void setExamCode(String examCode) {
        this.examCode = examCode;
    }

    public String getExamAnswer() {
        return examAnswer;
    }

    public void setExamAnswer(String examAnswer) {
        this.examAnswer = examAnswer;
    }

    public String getExamGrade() {
        return examGrade;
    }

    public void setExamGrade(String examGrade) {
        this.examGrade = examGrade;
    }

    public String getExamType() {
        return examType;
    }

    public void setExamType(String examType) {
        this.examType = examType;
    }

    public String getExamHardness() {
        return examHardness;
    }

    public void setExamHardness(String examHardness) {
        this.examHardness = examHardness;
    }

    public String getExamOptions() {
        return examOptions;
    }

    public void setExamOptions(String examOptions) {
        this.examOptions = examOptions;
    }

    public String getExamCreateTime() {
        return examCreateTime;
    }

    public void setExamCreateTime(String examCreateTime) {
        this.examCreateTime = examCreateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.examId;
    }

    @Override
    public String toString() {
        return "Examination{" +
        "examId=" + examId +
        ", examCode=" + examCode +
        ", examAnswer=" + examAnswer +
        ", examGrade=" + examGrade +
        ", examType=" + examType +
        ", examHardness=" + examHardness +
        ", examOptions=" + examOptions +
        ", examCreateTime=" + examCreateTime +
        "}";
    }
}
