package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色信息表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IRoleService extends IService<Role> {

}
