package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.Examination;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 试题表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IExaminationService extends IService<Examination> {

}
