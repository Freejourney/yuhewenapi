package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.OffersActivities;
import com.hbut603.yuhewenapi.api.service.IOffersActivitiesService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 优惠活动表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/offersActivities")
public class OffersActivitiesController {
	@Autowired
    private IOffersActivitiesService offersActivitiesService;

    @ApiOperation("查询所有活动")
    @GetMapping("/findall")
    public Object findAll() {
        List<OffersActivities> offersActivities = offersActivitiesService.list();
        return new JsonData().buildSuccess(offersActivities, 200);
    }

    @ApiOperation("查询单个活动")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "oaid")  String oaid) {
        QueryWrapper queryWrapper = new QueryWrapper<OffersActivities>();
        OffersActivities offersActivities = offersActivitiesService.getOne(queryWrapper.eq(true,"oa_id",oaid));
        return new JsonData().buildSuccess(offersActivities, 200);
    }

    @ApiOperation("新增活动")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody OffersActivities offersActivities) {
        boolean isSuccess = offersActivitiesService.save(offersActivities);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个活动信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个活动信息")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "oaid")  String oaid) {
        QueryWrapper queryWrapper = new QueryWrapper<OffersActivities>();
        boolean isSuccess = offersActivitiesService.remove(queryWrapper.eq(true,"oa_id",oaid));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
}