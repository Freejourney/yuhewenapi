package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.Informations;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 资讯与动态表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IInformationsService extends IService<Informations> {

}
