package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 页面菜单表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="Menu对象", description="页面菜单表")
public class Menu extends Model<Menu> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "menu_id", type = IdType.AUTO)
    private Integer menuId;

    @ApiModelProperty(value = "菜单编号")
    private String menuCode;

    @ApiModelProperty(value = "菜单父编号")
    private String menuPreCode;

    @ApiModelProperty(value = "所有父菜单编号")
    private String menuPreCodes;

    @ApiModelProperty(value = "菜单名称")
    private String menuName;

    @ApiModelProperty(value = "菜单图标")
    private String menuIcon;

    @ApiModelProperty(value = "URL地址")
    private String menuUrl;

    @ApiModelProperty(value = "菜单排序号")
    private Integer menuNum;

    @ApiModelProperty(value = "菜单层级")
    private Integer menuLevel;

    @ApiModelProperty(value = "是否是菜单 是否是菜单：1是，0不是")
    private Integer menuIsmenu;

    @ApiModelProperty(value = "菜单状态 菜单状态：1启用，0不启用")
    private Integer menuState;

    @ApiModelProperty(value = "备注")
    private String menuTips;

    private Integer menuIsparentmenu;


    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }

    public String getMenuPreCode() {
        return menuPreCode;
    }

    public void setMenuPreCode(String menuPreCode) {
        this.menuPreCode = menuPreCode;
    }

    public String getMenuPreCodes() {
        return menuPreCodes;
    }

    public void setMenuPreCodes(String menuPreCodes) {
        this.menuPreCodes = menuPreCodes;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public Integer getMenuNum() {
        return menuNum;
    }

    public void setMenuNum(Integer menuNum) {
        this.menuNum = menuNum;
    }

    public Integer getMenuLevel() {
        return menuLevel;
    }

    public void setMenuLevel(Integer menuLevel) {
        this.menuLevel = menuLevel;
    }

    public Integer getMenuIsmenu() {
        return menuIsmenu;
    }

    public void setMenuIsmenu(Integer menuIsmenu) {
        this.menuIsmenu = menuIsmenu;
    }

    public Integer getMenuState() {
        return menuState;
    }

    public void setMenuState(Integer menuState) {
        this.menuState = menuState;
    }

    public String getMenuTips() {
        return menuTips;
    }

    public void setMenuTips(String menuTips) {
        this.menuTips = menuTips;
    }

    public Integer getMenuIsparentmenu() {
        return menuIsparentmenu;
    }

    public void setMenuIsparentmenu(Integer menuIsparentmenu) {
        this.menuIsparentmenu = menuIsparentmenu;
    }

    @Override
    protected Serializable pkVal() {
        return this.menuId;
    }

    @Override
    public String toString() {
        return "Menu{" +
        "menuId=" + menuId +
        ", menuCode=" + menuCode +
        ", menuPreCode=" + menuPreCode +
        ", menuPreCodes=" + menuPreCodes +
        ", menuName=" + menuName +
        ", menuIcon=" + menuIcon +
        ", menuUrl=" + menuUrl +
        ", menuNum=" + menuNum +
        ", menuLevel=" + menuLevel +
        ", menuIsmenu=" + menuIsmenu +
        ", menuState=" + menuState +
        ", menuTips=" + menuTips +
        ", menuIsparentmenu=" + menuIsparentmenu +
        "}";
    }
}
