package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 用户角色表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="UrRelation对象", description="用户角色表")
public class UrRelation extends Model<UrRelation> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ur_id", type = IdType.AUTO)
    private Integer urId;

    @ApiModelProperty(value = "用户ID")
    private String urUserId;

    @ApiModelProperty(value = "角色ID")
    private Integer urRoleId;


    public Integer getUrId() {
        return urId;
    }

    public void setUrId(Integer urId) {
        this.urId = urId;
    }

    public String getUrUserId() {
        return urUserId;
    }

    public void setUrUserId(String urUserId) {
        this.urUserId = urUserId;
    }

    public Integer getUrRoleId() {
        return urRoleId;
    }

    public void setUrRoleId(Integer urRoleId) {
        this.urRoleId = urRoleId;
    }

    @Override
    protected Serializable pkVal() {
        return this.urId;
    }

    @Override
    public String toString() {
        return "UrRelation{" +
        "urId=" + urId +
        ", urUserId=" + urUserId +
        ", urRoleId=" + urRoleId +
        "}";
    }
}
