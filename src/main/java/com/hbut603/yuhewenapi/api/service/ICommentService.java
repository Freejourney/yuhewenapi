package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.Comment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 评论表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface ICommentService extends IService<Comment> {

}
