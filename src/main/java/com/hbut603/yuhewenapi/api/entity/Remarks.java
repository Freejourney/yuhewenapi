package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 评语表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="Remarks对象", description="评语表")
public class Remarks extends Model<Remarks> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "remark_id", type = IdType.AUTO)
    private Integer remarkId;

    @ApiModelProperty(value = "评论内容")
    private String remarkContent;


    public Integer getRemarkId() {
        return remarkId;
    }

    public void setRemarkId(Integer remarkId) {
        this.remarkId = remarkId;
    }

    public String getRemarkContent() {
        return remarkContent;
    }

    public void setRemarkContent(String remarkContent) {
        this.remarkContent = remarkContent;
    }

    @Override
    protected Serializable pkVal() {
        return this.remarkId;
    }

    @Override
    public String toString() {
        return "Remarks{" +
        "remarkId=" + remarkId +
        ", remarkContent=" + remarkContent +
        "}";
    }
}
