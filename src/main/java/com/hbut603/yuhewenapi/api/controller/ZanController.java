package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.Zan;
import com.hbut603.yuhewenapi.api.service.IZanService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 * 点赞表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/zan")
public class ZanController {
    @Autowired
    private IZanService zanService;

    @ApiOperation("查询所有赞")
    @GetMapping("/findall")
    public Object findAll() {
        List<Zan> zans = zanService.list();
        return new JsonData().buildSuccess(zans, 200);
    }

    @ApiOperation("查询单个赞")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "Z_id")  String account) {
        QueryWrapper queryWrapper = new QueryWrapper<Zan>();
        Zan zan = zanService.getOne(queryWrapper.eq(true,"zan_account",account));
        return new JsonData().buildSuccess(zan, 200);
    }

    @ApiOperation("新增赞")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody Zan zan) {
        boolean isSuccess = zanService.save(zan);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个赞信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个赞")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "Z_id")  String account) {
        QueryWrapper queryWrapper = new QueryWrapper<Zan>();
        boolean isSuccess = zanService.remove(queryWrapper.eq(true,"zan_account",account));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
}

