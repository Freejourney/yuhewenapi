package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.ShoppingCart;
import com.hbut603.yuhewenapi.api.service.IShoppingCartService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 购物车 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/shoppingCart")
public class ShoppingCartController {
    @Autowired
    private IShoppingCartService shoppingCartService;

    @ApiOperation("查询所有购物车信息")
    @GetMapping("/findall")
    public Object findAll() {
        List<ShoppingCart> shoppingCart = shoppingCartService.list();
        return new JsonData().buildSuccess(shoppingCart, 200);
    }

    @ApiOperation("查询单个用户的购物车")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "scuid") String uid) {
        QueryWrapper queryWrapper = new QueryWrapper<ShoppingCart>();
        List<ShoppingCart> shoppingCart = shoppingCartService.listObjs(queryWrapper.eq(true, "sc_user_id", uid));
        return new JsonData().buildSuccess(shoppingCart, 200);
    }

    @ApiOperation("新增用户购物车")
    @PostMapping(value = "/regist", consumes = "application/json")
    public Object regist(@RequestBody ShoppingCart shoppingCart) {
        boolean isSuccess = shoppingCartService.save(shoppingCart);
        return new JsonData().buildSuccess("" + isSuccess, 0);
    }

    @ApiOperation("修改单个用户购物车")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个用户购物车")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "scuid") String uid) {
        QueryWrapper queryWrapper = new QueryWrapper<ShoppingCart>();
        boolean isSuccess = shoppingCartService.remove(queryWrapper.eq(true, "sc_user_id", uid));
        return new JsonData().buildSuccess("" + isSuccess, 0);
    }
}

