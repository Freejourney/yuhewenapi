package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.Zan;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 点赞表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IZanService extends IService<Zan> {

}
