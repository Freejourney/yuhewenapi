package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.UbrRelation;
import com.hbut603.yuhewenapi.api.service.IUbrRelationService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 用户购买资源表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/ubrRelation")
public class UbrRelationController {
	@Autowired
    private IUbrRelationService ubrRelationService;

    @ApiOperation("查询所有购买资源")
    @GetMapping("/findall")
    public Object findAll() {
        List<UbrRelation> ubrRelation = ubrRelationService.list();
        return new JsonData().buildSuccess(ubrRelation, 200);
    }

    @ApiOperation("查询单个购买资源")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "ubrid")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<UbrRelation>();
        UbrRelation ubrRelation = ubrRelationService.getOne(queryWrapper.eq(true,"ubr_id",id));
        return new JsonData().buildSuccess(ubrRelation, 200);
    }

    @ApiOperation("新增购买资源")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody UbrRelation ubrRelation) {
        boolean isSuccess = ubrRelationService.save(ubrRelation);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个用户信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个用户")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "ubrid")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<UbrRelation>();
        boolean isSuccess = ubrRelationService.remove(queryWrapper.eq(true,"ubr_id",id));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
}

