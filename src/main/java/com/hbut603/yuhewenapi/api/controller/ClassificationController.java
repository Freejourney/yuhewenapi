package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.Classification;
import com.hbut603.yuhewenapi.api.service.IClassificationService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 分类表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/classification")
public class ClassificationController {
	@Autowired
    private IClassificationService classificationService;

    @ApiOperation("查询所有分类")
    @GetMapping("/findall")
    public Object findAll() {
        List<Classification> classification = classificationService.list();
        return new JsonData().buildSuccess(classification, 200);
    }

    @ApiOperation("查询单个分类")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "cid")  String cid) {
        QueryWrapper queryWrapper = new QueryWrapper<Classification>();
        Classification classification = classificationService.getOne(queryWrapper.eq(true,"class_id",cid));
        return new JsonData().buildSuccess(classification, 200);
    }

    @ApiOperation("新增分类")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody Classification classification) {
        boolean isSuccess = classificationService.save(classification);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个分类信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个分类")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "cid")  String cid) {
        QueryWrapper queryWrapper = new QueryWrapper<Classification>();
        boolean isSuccess = classificationService.remove(queryWrapper.eq(true,"class_id",cid));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
}

