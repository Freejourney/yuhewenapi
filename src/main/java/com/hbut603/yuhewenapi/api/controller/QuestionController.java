package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.Question;
import com.hbut603.yuhewenapi.api.service.IQuestionService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 * 提问表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/question")
public class QuestionController {
    @Autowired
    private IQuestionService questionService;

    @ApiOperation("查询所有提问")
    @GetMapping("/findall")
    public Object findAll() {
        List<Question> questions = questionService.list();
        return new JsonData().buildSuccess(questions, 200);
    }

    @ApiOperation("查询单个提问")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "Q_id")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<Question>();
        Question question = questionService.getOne(queryWrapper.eq(true,"question_id",id));
        return new JsonData().buildSuccess(question, 200);
    }

    @ApiOperation("新增提问")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody Question question) {
        boolean isSuccess = questionService.save(question);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个提问信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个提问")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "Q_id")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<Question>();
        boolean isSuccess = questionService.remove(queryWrapper.eq(true,"question_id",id));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
}

