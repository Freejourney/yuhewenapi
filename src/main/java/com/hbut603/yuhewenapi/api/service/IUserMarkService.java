package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.UserMark;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户评测成绩表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IUserMarkService extends IService<UserMark> {

}
