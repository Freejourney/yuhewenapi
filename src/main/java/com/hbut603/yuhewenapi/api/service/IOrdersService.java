package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.Orders;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IOrdersService extends IService<Orders> {

}
