package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.Remarks;
import com.hbut603.yuhewenapi.api.service.IRemarksService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 * 评语表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/remarks")
public class RemarksController {
    @Autowired
    private IRemarksService remarksService;

    @ApiOperation("查询所有提问")
    @GetMapping("/findall")
    public Object findAll() {
        List<Remarks> remarks = remarksService.list();
        return new JsonData().buildSuccess(remarks, 200);
    }

    @ApiOperation("查询单个提问")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "RM_id")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<Remarks>();
        Remarks remark = remarksService.getOne(queryWrapper.eq(true,"remark_id",id));
        return new JsonData().buildSuccess(remark, 200);
    }

    @ApiOperation("新增提问")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody Remarks remark) {
        boolean isSuccess = remarksService.save(remark);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个提问信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个提问")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "RM_id")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<Remarks>();
        boolean isSuccess = remarksService.remove(queryWrapper.eq(true,"remark_id",id));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
}

