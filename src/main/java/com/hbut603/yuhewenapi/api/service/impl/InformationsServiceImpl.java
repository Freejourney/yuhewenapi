package com.hbut603.yuhewenapi.api.service.impl;

import com.hbut603.yuhewenapi.api.entity.Informations;
import com.hbut603.yuhewenapi.api.mapper.InformationsMapper;
import com.hbut603.yuhewenapi.api.service.IInformationsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资讯与动态表 服务实现类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@Service
public class InformationsServiceImpl extends ServiceImpl<InformationsMapper, Informations> implements IInformationsService {

}
