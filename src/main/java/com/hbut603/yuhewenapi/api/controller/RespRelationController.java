package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.RespRelation;
import com.hbut603.yuhewenapi.api.service.IRespRelationService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 资源权限表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/respRelation")
public class RespRelationController {
	@Autowired
    private IRespRelationService respRelationService;

    @ApiOperation("查询所有资源权限")
    @GetMapping("/findall")
    public Object findAll() {
        List<RespRelation> respRelation = respRelationService.list();
        return new JsonData().buildSuccess(respRelation, 200);
    }

    @ApiOperation("查询单个资源权限")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "respid")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<RespRelation>();
        RespRelation respRelation = respRelationService.getOne(queryWrapper.eq(true,"resp_id",id));
        return new JsonData().buildSuccess(respRelation, 200);
    }

    @ApiOperation("新增资源权限")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody RespRelation respRelation) {
        boolean isSuccess = respRelationService.save(respRelation);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个资源权限")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个资源权限")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "respid")  String id) {
        QueryWrapper queryWrapper = new QueryWrapper<RespRelation>();
        boolean isSuccess = respRelationService.remove(queryWrapper.eq(true,"resp_id",id));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
}
