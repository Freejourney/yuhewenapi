package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.ChapterContent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 章节内容表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IChapterContentService extends IService<ChapterContent> {

}
