package com.hbut603.yuhewenapi.api.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 订单详情表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="OrderDetail对象", description="订单详情表")
public class OrderDetail extends Model<OrderDetail> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "od_id", type = IdType.AUTO)
    private Integer odId;

    @ApiModelProperty(value = "订单编号")
    private String odOrderCode;

    @ApiModelProperty(value = "商品编号")
    private String odProductsCode;

    @ApiModelProperty(value = "商品名称")
    private String odProductsName;

    @ApiModelProperty(value = "商品价格")
    private BigDecimal odProductsPrice;

    @ApiModelProperty(value = "商品类型")
    private Integer odProductsType;

    @ApiModelProperty(value = "折扣比例")
    private BigDecimal odDiscountPercent;

    @ApiModelProperty(value = "折扣金额")
    private BigDecimal odDiscountPrice;

    @ApiModelProperty(value = "小计金额")
    private BigDecimal odSubtotalAmount;

    @ApiModelProperty(value = "商品是否有效")
    private Integer odIsvalide;

    @ApiModelProperty(value = "客户商品备注")
    private String odUserTips;


    public Integer getOdId() {
        return odId;
    }

    public void setOdId(Integer odId) {
        this.odId = odId;
    }

    public String getOdOrderCode() {
        return odOrderCode;
    }

    public void setOdOrderCode(String odOrderCode) {
        this.odOrderCode = odOrderCode;
    }

    public String getOdProductsCode() {
        return odProductsCode;
    }

    public void setOdProductsCode(String odProductsCode) {
        this.odProductsCode = odProductsCode;
    }

    public String getOdProductsName() {
        return odProductsName;
    }

    public void setOdProductsName(String odProductsName) {
        this.odProductsName = odProductsName;
    }

    public BigDecimal getOdProductsPrice() {
        return odProductsPrice;
    }

    public void setOdProductsPrice(BigDecimal odProductsPrice) {
        this.odProductsPrice = odProductsPrice;
    }

    public Integer getOdProductsType() {
        return odProductsType;
    }

    public void setOdProductsType(Integer odProductsType) {
        this.odProductsType = odProductsType;
    }

    public BigDecimal getOdDiscountPercent() {
        return odDiscountPercent;
    }

    public void setOdDiscountPercent(BigDecimal odDiscountPercent) {
        this.odDiscountPercent = odDiscountPercent;
    }

    public BigDecimal getOdDiscountPrice() {
        return odDiscountPrice;
    }

    public void setOdDiscountPrice(BigDecimal odDiscountPrice) {
        this.odDiscountPrice = odDiscountPrice;
    }

    public BigDecimal getOdSubtotalAmount() {
        return odSubtotalAmount;
    }

    public void setOdSubtotalAmount(BigDecimal odSubtotalAmount) {
        this.odSubtotalAmount = odSubtotalAmount;
    }

    public Integer getOdIsvalide() {
        return odIsvalide;
    }

    public void setOdIsvalide(Integer odIsvalide) {
        this.odIsvalide = odIsvalide;
    }

    public String getOdUserTips() {
        return odUserTips;
    }

    public void setOdUserTips(String odUserTips) {
        this.odUserTips = odUserTips;
    }

    @Override
    protected Serializable pkVal() {
        return this.odId;
    }

    @Override
    public String toString() {
        return "OrderDetail{" +
        "odId=" + odId +
        ", odOrderCode=" + odOrderCode +
        ", odProductsCode=" + odProductsCode +
        ", odProductsName=" + odProductsName +
        ", odProductsPrice=" + odProductsPrice +
        ", odProductsType=" + odProductsType +
        ", odDiscountPercent=" + odDiscountPercent +
        ", odDiscountPrice=" + odDiscountPrice +
        ", odSubtotalAmount=" + odSubtotalAmount +
        ", odIsvalide=" + odIsvalide +
        ", odUserTips=" + odUserTips +
        "}";
    }
}
