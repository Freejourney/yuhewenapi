package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 课程表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="Lesson对象", description="课程表")
public class Lesson extends Model<Lesson> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "lesson_id", type = IdType.AUTO)
    private Integer lessonId;

    @ApiModelProperty(value = "课程编号")
    private String lessonCode;

    @ApiModelProperty(value = "课程名称")
    private String lessonName;

    @ApiModelProperty(value = "课程图片")
    private String lessonImage;

    @ApiModelProperty(value = "课程价格")
    private String lessonPrice;

    @ApiModelProperty(value = "主讲老师")
    private String lessonTeacher;

    @ApiModelProperty(value = "辅导老师")
    private String lessonTa;

    @ApiModelProperty(value = "课程类型 课程类型：1：直播，2：视频")
    private Integer lessonType;

    @ApiModelProperty(value = "课程学期")
    private String lessonTerm;

    @ApiModelProperty(value = "课程年级")
    private String lessonGrade;

    @ApiModelProperty(value = "课程状态")
    private String lessonState;

    @ApiModelProperty(value = "课程内容 课程章节信息表编号")
    private String lessonContents;

    @ApiModelProperty(value = "课程介绍")
    private String lessonIntro;

    @ApiModelProperty(value = "上架时间")
    private String lessonCreateTime;

    @ApiModelProperty(value = "开课时间")
    private String lessonStartTime;

    @ApiModelProperty(value = "课程直播码")
    private String lessonLiveCode;


    public Integer getLessonId() {
        return lessonId;
    }

    public void setLessonId(Integer lessonId) {
        this.lessonId = lessonId;
    }

    public String getLessonCode() {
        return lessonCode;
    }

    public void setLessonCode(String lessonCode) {
        this.lessonCode = lessonCode;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public String getLessonImage() {
        return lessonImage;
    }

    public void setLessonImage(String lessonImage) {
        this.lessonImage = lessonImage;
    }

    public String getLessonPrice() {
        return lessonPrice;
    }

    public void setLessonPrice(String lessonPrice) {
        this.lessonPrice = lessonPrice;
    }

    public String getLessonTeacher() {
        return lessonTeacher;
    }

    public void setLessonTeacher(String lessonTeacher) {
        this.lessonTeacher = lessonTeacher;
    }

    public String getLessonTa() {
        return lessonTa;
    }

    public void setLessonTa(String lessonTa) {
        this.lessonTa = lessonTa;
    }

    public Integer getLessonType() {
        return lessonType;
    }

    public void setLessonType(Integer lessonType) {
        this.lessonType = lessonType;
    }

    public String getLessonTerm() {
        return lessonTerm;
    }

    public void setLessonTerm(String lessonTerm) {
        this.lessonTerm = lessonTerm;
    }

    public String getLessonGrade() {
        return lessonGrade;
    }

    public void setLessonGrade(String lessonGrade) {
        this.lessonGrade = lessonGrade;
    }

    public String getLessonState() {
        return lessonState;
    }

    public void setLessonState(String lessonState) {
        this.lessonState = lessonState;
    }

    public String getLessonContents() {
        return lessonContents;
    }

    public void setLessonContents(String lessonContents) {
        this.lessonContents = lessonContents;
    }

    public String getLessonIntro() {
        return lessonIntro;
    }

    public void setLessonIntro(String lessonIntro) {
        this.lessonIntro = lessonIntro;
    }

    public String getLessonCreateTime() {
        return lessonCreateTime;
    }

    public void setLessonCreateTime(String lessonCreateTime) {
        this.lessonCreateTime = lessonCreateTime;
    }

    public String getLessonStartTime() {
        return lessonStartTime;
    }

    public void setLessonStartTime(String lessonStartTime) {
        this.lessonStartTime = lessonStartTime;
    }

    public String getLessonLiveCode() {
        return lessonLiveCode;
    }

    public void setLessonLiveCode(String lessonLiveCode) {
        this.lessonLiveCode = lessonLiveCode;
    }

    @Override
    protected Serializable pkVal() {
        return this.lessonId;
    }

    @Override
    public String toString() {
        return "Lesson{" +
        "lessonId=" + lessonId +
        ", lessonCode=" + lessonCode +
        ", lessonName=" + lessonName +
        ", lessonImage=" + lessonImage +
        ", lessonPrice=" + lessonPrice +
        ", lessonTeacher=" + lessonTeacher +
        ", lessonTa=" + lessonTa +
        ", lessonType=" + lessonType +
        ", lessonTerm=" + lessonTerm +
        ", lessonGrade=" + lessonGrade +
        ", lessonState=" + lessonState +
        ", lessonContents=" + lessonContents +
        ", lessonIntro=" + lessonIntro +
        ", lessonCreateTime=" + lessonCreateTime +
        ", lessonStartTime=" + lessonStartTime +
        ", lessonLiveCode=" + lessonLiveCode +
        "}";
    }
}
