package com.hbut603.yuhewenapi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.hbut603.yuhewenapi.api"})
@SpringBootApplication
@MapperScan("com.hbut603.yuhewenapi.api.mapper")
public class YuhewenapiApplication extends SpringBootServletInitializer{
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder){
		return builder.sources(YuhewenapiApplication.class);
	}
	public static void main(String[] args) {
		SpringApplication.run(YuhewenapiApplication.class, args);
	}
}
