package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 章节内容表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="ChapterContent对象", description="章节内容表")
public class ChapterContent extends Model<ChapterContent> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "chapter_id", type = IdType.AUTO)
    private Integer chapterId;

    @ApiModelProperty(value = "课程编号")
    private String chapterLessonId;

    @ApiModelProperty(value = "章编号")
    private String chapterUnitId;

    @ApiModelProperty(value = "章名称")
    private String chapterUnitName;

    @ApiModelProperty(value = "节编号")
    private String chapterSectionId;

    @ApiModelProperty(value = "节名称")
    private String chapterSectionName;

    @ApiModelProperty(value = "课程视频连接")
    private String lessonUrl;


    public Integer getChapterId() {
        return chapterId;
    }

    public void setChapterId(Integer chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterLessonId() {
        return chapterLessonId;
    }

    public void setChapterLessonId(String chapterLessonId) {
        this.chapterLessonId = chapterLessonId;
    }

    public String getChapterUnitId() {
        return chapterUnitId;
    }

    public void setChapterUnitId(String chapterUnitId) {
        this.chapterUnitId = chapterUnitId;
    }

    public String getChapterUnitName() {
        return chapterUnitName;
    }

    public void setChapterUnitName(String chapterUnitName) {
        this.chapterUnitName = chapterUnitName;
    }

    public String getChapterSectionId() {
        return chapterSectionId;
    }

    public void setChapterSectionId(String chapterSectionId) {
        this.chapterSectionId = chapterSectionId;
    }

    public String getChapterSectionName() {
        return chapterSectionName;
    }

    public void setChapterSectionName(String chapterSectionName) {
        this.chapterSectionName = chapterSectionName;
    }

    public String getLessonUrl() {
        return lessonUrl;
    }

    public void setLessonUrl(String lessonUrl) {
        this.lessonUrl = lessonUrl;
    }

    @Override
    protected Serializable pkVal() {
        return this.chapterId;
    }

    @Override
    public String toString() {
        return "ChapterContent{" +
        "chapterId=" + chapterId +
        ", chapterLessonId=" + chapterLessonId +
        ", chapterUnitId=" + chapterUnitId +
        ", chapterUnitName=" + chapterUnitName +
        ", chapterSectionId=" + chapterSectionId +
        ", chapterSectionName=" + chapterSectionName +
        ", lessonUrl=" + lessonUrl +
        "}";
    }
}
