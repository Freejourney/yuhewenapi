package com.hbut603.yuhewenapi.api.mapper;

import com.hbut603.yuhewenapi.api.entity.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单详情表 Mapper 接口
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {

}
