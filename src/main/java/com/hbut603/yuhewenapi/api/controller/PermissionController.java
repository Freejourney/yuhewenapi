package com.hbut603.yuhewenapi.api.controller;


import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.Permission;
import com.hbut603.yuhewenapi.api.service.IPermissionService;
import com.hbut603.yuhewenapi.api.utils.JsonData;

import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 权限信息表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/permission")
public class PermissionController {
	@Autowired
    private IPermissionService permissionService;

    @ApiOperation("查询所有权限信息")
    @GetMapping("/findall")
    public Object findAll() {
        List<Permission> permission = permissionService.list();
        return new JsonData().buildSuccess(permission, 200);
    }

    @ApiOperation("查询单个权限信息")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "pnum")  int num) {
        QueryWrapper queryWrapper = new QueryWrapper<Permission>();
        Permission permission = permissionService.getOne(queryWrapper.eq(true,"permission_num",num));
        return new JsonData().buildSuccess(permission, 200);
    }

    @ApiOperation("新增权限")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody Permission permission) {
        boolean isSuccess = permissionService.save(permission);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个角色信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个角色")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "pnum")  String num) {
        QueryWrapper queryWrapper = new QueryWrapper<Permission>();
        boolean isSuccess = permissionService.remove(queryWrapper.eq(true,"permission_num",num));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
}


