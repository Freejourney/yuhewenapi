package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 权限信息表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IPermissionService extends IService<Permission> {

}
