package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 用户购买资源表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="UbrRelation对象", description="用户购买资源表")
public class UbrRelation extends Model<UbrRelation> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "ubr_id", type = IdType.AUTO)
    private Integer ubrId;

    @ApiModelProperty(value = "用户ID")
    private String ubrUserId;

    @ApiModelProperty(value = "订单号")
    private String ubrOrderId;

    @ApiModelProperty(value = "资源ID")
    private String ubrResId;

    @ApiModelProperty(value = "购买时间")
    private LocalDateTime ubrBuyTime;

    @ApiModelProperty(value = "当前状态")
    private String ubrResState;


    public Integer getUbrId() {
        return ubrId;
    }

    public void setUbrId(Integer ubrId) {
        this.ubrId = ubrId;
    }

    public String getUbrUserId() {
        return ubrUserId;
    }

    public void setUbrUserId(String ubrUserId) {
        this.ubrUserId = ubrUserId;
    }

    public String getUbrOrderId() {
        return ubrOrderId;
    }

    public void setUbrOrderId(String ubrOrderId) {
        this.ubrOrderId = ubrOrderId;
    }

    public String getUbrResId() {
        return ubrResId;
    }

    public void setUbrResId(String ubrResId) {
        this.ubrResId = ubrResId;
    }

    public LocalDateTime getUbrBuyTime() {
        return ubrBuyTime;
    }

    public void setUbrBuyTime(LocalDateTime ubrBuyTime) {
        this.ubrBuyTime = ubrBuyTime;
    }

    public String getUbrResState() {
        return ubrResState;
    }

    public void setUbrResState(String ubrResState) {
        this.ubrResState = ubrResState;
    }

    @Override
    protected Serializable pkVal() {
        return this.ubrId;
    }

    @Override
    public String toString() {
        return "UbrRelation{" +
        "ubrId=" + ubrId +
        ", ubrUserId=" + ubrUserId +
        ", ubrOrderId=" + ubrOrderId +
        ", ubrResId=" + ubrResId +
        ", ubrBuyTime=" + ubrBuyTime +
        ", ubrResState=" + ubrResState +
        "}";
    }
}
