package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.Lesson;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface ILessonService extends IService<Lesson> {

}
