package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 回答表
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="Answer对象", description="回答表")
public class Answer extends Model<Answer> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "answer_id", type = IdType.AUTO)
    private Integer answerId;

    @ApiModelProperty(value = "回答编号")
    private String answerCode;

    @ApiModelProperty(value = "问题id")
    private String answerQuestionId;

    @ApiModelProperty(value = "回答内容")
    private String answerContent;

    @ApiModelProperty(value = "回答用户")
    private String answerUser;

    @ApiModelProperty(value = "回答时间")
    private String answerCreateTime;

    @ApiModelProperty(value = "更新时间")
    private String answerUpdateTime;


    public Integer getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Integer answerId) {
        this.answerId = answerId;
    }

    public String getAnswerCode() {
        return answerCode;
    }

    public void setAnswerCode(String answerCode) {
        this.answerCode = answerCode;
    }

    public String getAnswerQuestionId() {
        return answerQuestionId;
    }

    public void setAnswerQuestionId(String answerQuestionId) {
        this.answerQuestionId = answerQuestionId;
    }

    public String getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(String answerContent) {
        this.answerContent = answerContent;
    }

    public String getAnswerUser() {
        return answerUser;
    }

    public void setAnswerUser(String answerUser) {
        this.answerUser = answerUser;
    }

    public String getAnswerCreateTime() {
        return answerCreateTime;
    }

    public void setAnswerCreateTime(String answerCreateTime) {
        this.answerCreateTime = answerCreateTime;
    }

    public String getAnswerUpdateTime() {
        return answerUpdateTime;
    }

    public void setAnswerUpdateTime(String answerUpdateTime) {
        this.answerUpdateTime = answerUpdateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.answerId;
    }

    @Override
    public String toString() {
        return "Answer{" +
        "answerId=" + answerId +
        ", answerCode=" + answerCode +
        ", answerQuestionId=" + answerQuestionId +
        ", answerContent=" + answerContent +
        ", answerUser=" + answerUser +
        ", answerCreateTime=" + answerCreateTime +
        ", answerUpdateTime=" + answerUpdateTime +
        "}";
    }
}
