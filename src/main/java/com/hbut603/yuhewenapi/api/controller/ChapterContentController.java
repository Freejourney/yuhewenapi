package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.ChapterContent;
import com.hbut603.yuhewenapi.api.service.IChapterContentService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 章节内容表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/chapterContent")
public class ChapterContentController {
	@Autowired
    private IChapterContentService chapterContentService;

    @ApiOperation("查询所有章节")
    @GetMapping("/findall")
    public Object findAll() {
        List<ChapterContent> chapterContent = chapterContentService.list();
        return new JsonData().buildSuccess(chapterContent, 200);
    }

    @ApiOperation("查询单个章节")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "ctid")  String ctid) {
        QueryWrapper queryWrapper = new QueryWrapper<ChapterContent>();
        ChapterContent chapterContent = chapterContentService.getOne(queryWrapper.eq(true,"chapter_id",ctid));
        return new JsonData().buildSuccess(chapterContent, 200);
    }

    @ApiOperation("新增章节")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody ChapterContent chapterContent) {
        boolean isSuccess = chapterContentService.save(chapterContent);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个章节信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个章节信息")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "ctid")  String ctid) {
        QueryWrapper queryWrapper = new QueryWrapper<ChapterContent>();
        boolean isSuccess = chapterContentService.remove(queryWrapper.eq(true,"chapter_id",ctid));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
}
