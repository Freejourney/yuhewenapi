package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.Answer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 回答表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IAnswerService extends IService<Answer> {

}
