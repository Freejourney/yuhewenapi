package com.hbut603.yuhewenapi.api.service.impl;

import com.hbut603.yuhewenapi.api.entity.Answer;
import com.hbut603.yuhewenapi.api.mapper.AnswerMapper;
import com.hbut603.yuhewenapi.api.service.IAnswerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 回答表 服务实现类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@Service
public class AnswerServiceImpl extends ServiceImpl<AnswerMapper, Answer> implements IAnswerService {

}
