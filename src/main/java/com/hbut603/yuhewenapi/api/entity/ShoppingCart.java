package com.hbut603.yuhewenapi.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 购物车
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@ApiModel(value="ShoppingCart对象", description="购物车")
public class ShoppingCart extends Model<ShoppingCart> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "sc_id", type = IdType.AUTO)
    private Integer scId;

    @ApiModelProperty(value = "用户编号")
    private String scUserId;

    @ApiModelProperty(value = "商品编号")
    private String scProductId;

    @ApiModelProperty(value = "是否有效")
    private Integer scIsvalid;

    @ApiModelProperty(value = "购买数量")
    private Integer scQuantity;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime scCreateTime;


    public Integer getScId() {
        return scId;
    }

    public void setScId(Integer scId) {
        this.scId = scId;
    }

    public String getScUserId() {
        return scUserId;
    }

    public void setScUserId(String scUserId) {
        this.scUserId = scUserId;
    }

    public String getScProductId() {
        return scProductId;
    }

    public void setScProductId(String scProductId) {
        this.scProductId = scProductId;
    }

    public Integer getScIsvalid() {
        return scIsvalid;
    }

    public void setScIsvalid(Integer scIsvalid) {
        this.scIsvalid = scIsvalid;
    }

    public Integer getScQuantity() {
        return scQuantity;
    }

    public void setScQuantity(Integer scQuantity) {
        this.scQuantity = scQuantity;
    }

    public LocalDateTime getScCreateTime() {
        return scCreateTime;
    }

    public void setScCreateTime(LocalDateTime scCreateTime) {
        this.scCreateTime = scCreateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.scId;
    }

    @Override
    public String toString() {
        return "ShoppingCart{" +
        "scId=" + scId +
        ", scUserId=" + scUserId +
        ", scProductId=" + scProductId +
        ", scIsvalid=" + scIsvalid +
        ", scQuantity=" + scQuantity +
        ", scCreateTime=" + scCreateTime +
        "}";
    }
}
