package com.hbut603.yuhewenapi.api.mapper;

import com.hbut603.yuhewenapi.api.entity.Classification;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 分类表 Mapper 接口
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface ClassificationMapper extends BaseMapper<Classification> {

}
