package com.hbut603.yuhewenapi.api.service;

import com.hbut603.yuhewenapi.api.entity.File;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 文件资料表 服务类
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
public interface IFileService extends IService<File> {

}
