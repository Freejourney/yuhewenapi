package com.hbut603.yuhewenapi.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hbut603.yuhewenapi.api.entity.File;
import com.hbut603.yuhewenapi.api.entity.User;
import com.hbut603.yuhewenapi.api.service.IFileService;
import com.hbut603.yuhewenapi.api.service.IUserService;
import com.hbut603.yuhewenapi.api.utils.JsonData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 文件资料表 前端控制器
 * </p>
 *
 * @author ThornSun
 * @since 2018-11-21
 */
@RestController
@RequestMapping("/api/file")
public class FileController {
	@Autowired
    private IFileService fileService;

    @ApiOperation("查询所有下载文件")
    @GetMapping("/findall")
    public Object findAll() {
        List<File> file = fileService.list();
        return new JsonData().buildSuccess(file, 200);
    }

    @ApiOperation("查询单个文件")
    @GetMapping("/findbyid")
    public Object findById(@RequestParam(value = "fid")  String fid) {
        QueryWrapper queryWrapper = new QueryWrapper<File>();
        File file = fileService.getOne(queryWrapper.eq(true,"file_id",fid));
        return new JsonData().buildSuccess(file, 200);
    }

    @ApiOperation("新增文件")
    @PostMapping(value= "/regist", consumes = "application/json")
    public Object regist(@RequestBody File file) {
        boolean isSuccess = fileService.save(file);
        return new JsonData().buildSuccess(""+isSuccess,0);
    }

    @ApiOperation("修改单个文件信息")
    @PutMapping("/update")
    public Object update() {

        return new JsonData().buildSuccess("", 0);
    }

    @ApiOperation("删除单个文件")
    @DeleteMapping("/delete")
    public Object delete(@RequestParam(value = "fid")  String fid) {
        QueryWrapper queryWrapper = new QueryWrapper<User>();
        boolean isSuccess = fileService.remove(queryWrapper.eq(true,"file_id",fid));
        return new JsonData().buildSuccess(""+isSuccess, 0);
    }
}

